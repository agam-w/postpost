const router = require('express').Router();
const postController = require('../controllers/postController.js');
const authenticate = require('../helpers/authenticate/authenticate.js')

router.post('/', authenticate, postController.create)
router.get('/:id', authenticate, postController.show)
router.put('/:id', authenticate, postController.update)
router.delete('/:id', authenticate, postController.delete)

module.exports = router