const router = require('express').Router();
const userController = require('../controllers/userController.js');
const authenticate = require('../helpers/authenticate/authenticate.js')

router.post('/', userController.create);
router.get('/show', authenticate, userController.show);
router.post('/login', userController.login);
router.get('/', userController.currentUser);

module.exports = router;