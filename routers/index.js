const router = require('express').Router();

const userRouter = require('./userRouter.js') 
const postRouter = require('./postRouter.js')
const uploadRouter = require('./uploadRouter')

router.use('/user', userRouter)
router.use('/post', postRouter)
router.use('/upload', uploadRouter)

module.exports = router;