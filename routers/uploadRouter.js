const router = require('express').Router();
const uploadController = require('../controllers/uploadController.js');
const authenticate = require('../helpers/authenticate/authenticate.js')

router.post('/', authenticate, uploadController)

module.exports = router;