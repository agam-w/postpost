var ImageKit = require('imagekit');
var {app, imagekit} = require('../index.js')
const {success, error} = require('../helpers/response/response.js')
const multer  = require('multer')

const User = require('../models/user.js')


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/upload')
    },
    filename: function (req, file, cb) {
        var filetype = '';
        if (file.mimetype === 'image/gif') {
            filetype = 'gif';
        }
        else if (file.mimetype === 'image/png') {
            filetype = 'png';
        }
        else if (file.mimetype === 'image/jpeg') {
            filetype = 'jpg';
        }
        cb(null, file.fieldname + '-' + Date.now()+'.'+filetype)
    }
})
 
var upload = multer().single('img')

module.exports= 

    // upload.single('image'), //bisa ditulis seperti ini juga
    function(req,res){
        upload(req, res, function(err){
            var file = req.file
            if (!file) {
                return res.status(422).json(error("please upload file"))
            }

        var uploadPromise;
        uploadPromise = imagekit.upload(req.file.buffer.toString('base64'), {
            "filename" : req.file.originalname,
            "folder"   : "/images"
        });
       
        //handle upload success and failure
        uploadPromise.then((result)=>{
            User.findByIdAndUpdate(req.user._id,{image:result.url}, (err, updt)=>{
                if(err) return res.status(422).json(error("can't update url image"))   
                //res.status(200).json(success(updt, "url image has been update"))
                User.findById(updt._id, (err, showUser)=>{
                    if(err) return res.status(422).json(error("can't update url image"))   
                    res.status(200).json(success(showUser, "url image has been update"))
                })
            })
            
        })
        .catch (err=>{
            res.status(400).json(error("can't upload file"))
        })
        })
    }
