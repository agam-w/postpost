const express = require('express');
const app = express();
const {success, error} = require('../helpers/response/response.js')
const multer  = require('multer')
var ImageKit = require('imagekit');

var imagekit = new ImageKit({
   "imagekitId" : "postxxx",       
   "apiKey"     : "myapikey",       
   "apiSecret"  : "myapisecret", 
});

/*
    Returns
    https://demo.imagekit.io/img/tr:h-100,w-200,cm-resize:b-5_FF0000/default-image.jpg
*/
var url = imagekit.image("default-image.jpg")
              .transform({HEIGHT : 100, WIDTH : 200, CROP_MODE : "resize"})
              .transform({BORDER : "5_FF0000"})
              .url();


/*
    Uploads an image with the name my-image in the folder /images in your storage
 */
var uploadPromise = imagekit.upload(imageAsBase64String, {
                                "filename" : "my-image",
                                "folder" : "/images"
                            });

/*
    Uploads an image by specifying the HTTP URL
 */
var uploadPromise = imagekit.uploadViaURL("http://www.example.com/image.jpg", {
                                "filename" : "my-image",
                                "folder" : "/images"
                            });  
                            
uploadPromise.then(function(resp) {
    //handle upload success here    
}, function(err) {
    //handle upload failure here
});

/*
    Delete an image by specifying the image path that is returned in respone of upload API
 */
var deletePromise = imagekit.deleteFile("images/my-image.jpg");  
                            
deletePromise.then(function(resp) {
    //handle delete success here    
}, function(err) {
    //handle delete failure here
});
                      
/*
  Purge an image URL
*/
var purgePromise = imagekit.purgeFile("https://ik.imagekit.io/yourimagekitId/images/my-image.jpg");  
                          
purgePromise.then(function(resp) {
  //handle purge success here    
}, function(err) {
  //handle purge failure here
});

/*
    List uploaded files in media library.

    Provide skip and limit as paramters
 */
var listMediaFiles = imagekit.listUploadedMediaFiles(0,1000);
                            
listMediaFiles.then(function(resp) {
    //handle array of results
}, function(err) {
    //handle failure here
});