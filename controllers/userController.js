const User = require('../models/user.js')
const express = require('express');
const app = express();
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');

const {success, error} = require('../helpers/response/response.js')

const saltRounds = 10;

module.exports = {
    create(req, res){ 

        bcrypt.hash(req.body.password, saltRounds, (err, hash)=>{
            if(err) return res.status(422).json(error("can't hashing password"))
            User.create({
                name        : req.body.name,
                email       : req.body.email,
                password    : hash

            }, (err, data)=>{
                if(err) return res.status(422).json(error("can't make new user")) 
                res.status(201).json(success(data, "created new user"))
            })

        })
    },
    
    show(req, res){

        User.findById(req.user._id).populate('posts')
            .exec(function (err, post) {

                if (err) return req.status(403).json(error("there is no user found"));
                res.status(200).json(success(post, "show user selected"));
        });
    },

    login(req, res){

        User.findOne({email: req.body.email}).then(function(user){
            if(!user){
                return res.status(400).json(error("invalid user"))
            } else {

                bcrypt.compare(req.body.password, user.password, function (err, result) {
                    if (result == true) {

                        jwt.sign({ _id: user._id }, process.env.SECRET_KEY, function(err, token) {
                            res.status(200).json(success(token, "token created"));
                        });
                    } else {
                        res.status(403).json(error('Incorrect password'));
                    }

                })
            }
        })
    },
    
    currentUser(req, res){

        User.findById(jwt.verify(req.headers.authorization, 'xxx')._id).exec(function (err, user) {
            if (err) return res.status(400).json(error("Can't put from header"));
            res.status(200).json(user);
        });
        
    }
}
