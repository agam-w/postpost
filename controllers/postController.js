const Post = require('../models/post.js')
const User = require('../models/user.js')
const {success, error} = require('../helpers/response/response.js')

module.exports={
    create(req, res){
        Post.create(req.body, function (err, data){

            if (err) return res.status(400).json(error("Can't make post"))
            User.findById(req.user._id, (err, user)=>{

                if (err) return res.status(400).json(error("can't find user"))
                user.posts.push(data)
                user.save()
                data.user=req.user._id
                data.save()
            })
            res.status(201).json(success(data, "post created"))

        })
    },
    show(req, res){

        Post.findById(req.params.id).populate('user', '_id').exec ((err, data)=>{
            if (data.user._id != req.user._id) return res.status(404).json(error("this is not your post!"))
            res.status(200).json(success(data, "show post"))
        }) 

    },
    update(req, res){
        Post.findById(req.params.id, (err, data)=>{

            if (data.user._id != req.user._id) 
            return res.status(404).json(error("this is not your post!"))
            Post.findByIdAndUpdate(data._id,req.body, (err, updt)=>{

                if(err) return res.status(422).json(error("post can't be update"))
                res.status(200).json(success(req.body, "post has been update"))
            })

        })
    },
    delete(req, res){
        Post.findById(req.params.id).populate('user', '_id').exec ((err, data)=>{

            if (data.user._id != req.user._id) 
            return res.status(404).json(error("this is not your post!"))
            Post.findByIdAndDelete(data._id, (err, post)=>{

                if (err) return res.status(422).json(error("there is nothing to delete"))
                User.update({ _id: req.user._id }, { $pullAll: { posts: [req.params.id] } }, 
                    { safe: true, multi:true }, function(err, obj) {

                        if (err) return res.status(422).json(error("can't delete from array"))
                        res.status(200).json(success(post ,"post deleted"))
                });
                
            })
        }) 
    }
}
