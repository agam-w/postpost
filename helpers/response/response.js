const success = (results, message)=>{
    return{
        success: true,
        message: message,
        result: results
    }
}

const error = (err)=>{
    return{
        success: false,
        result: err
    }
}

module.exports = {success,error};
