const jwt = require('jsonwebtoken');
const {success, error} = require('../response/response.js')

module.exports =  function (req, res, next){

    const token1 = req.header('authorization')
    if(!token1) return res.status(401).json(error("can't put from header"))

    const token0 = req.header('authorization').split(" ")
    const token = token0[1]
 
    try {
        const verified = jwt.verify(token, process.env.SECRET_KEY)
        req.user = verified
        next()
    }

    catch (err){
        res.status(401).json(error("Invalid Token"))
    }
}
