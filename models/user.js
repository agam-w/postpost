const mongoose = require('mongoose')
var uniqueValidator = require('mongoose-unique-validator');
require('mongoose-type-email')
const Schema = mongoose.Schema

const userSchema = new Schema({

    name    : {type   :'string',
              required: true,
              unique  : true},
    email   : {type   :mongoose.SchemaTypes.Email,
              required: true,
              unique  : true},
    password: {type   : 'string',
              required: true},
    posts   : [{type  : Schema.Types.ObjectId, 
              ref     : 'Post'}],
    image   : {required: false}

  });

  userSchema.plugin(uniqueValidator);
  module.exports = mongoose.model('User', userSchema);